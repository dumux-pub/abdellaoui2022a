dune_symlink_to_source_files(FILES injection_checkpoints.dat injection_type.dat params.input)

# compile MICP simplified chemistry column setup
dumux_add_test(NAME simplemicproad
               SOURCES main.cc)
