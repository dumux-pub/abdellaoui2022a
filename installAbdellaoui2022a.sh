#!/bin/bash

echo "*********************************************************************************************"
echo "(1/2) Cloning repositories. This may take a while. Make sure to be connected to the internet."
echo "*********************************************************************************************"
# the DUNE core modules
for MOD in common geometry grid localfunctions istl; do
    if [ ! -d "dune-$MOD" ]; then
        git clone https://gitlab.dune-project.org/core/dune-$MOD.git
    else
        echo "Skip cloning dune-$MOD because the folder already exists."
        cd dune-$MOD
        git checkout releases/2.8
        cd ..
    fi
done


# dumux
if [ ! -d "dumux" ]; then
    git clone https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git
else
    echo "Skip cloning dumux because the folder already exists."
    cd dumux
    git checkout master
    cd ..
fi

# The module Abdellaoui2022a
if [ ! -d "abdellaoui2022a" ]; then
    git clone https://git.iws.uni-stuttgart.de/dumux-pub/abdellaoui2022a.git
else
    echo "Skip cloning abdellaoui2022a because the folder already exists."
    cd abdellaoui2022a
    git checkout master
    cd ..
fi

### run dunecontrol
./dune-common/bin/dunecontrol --opts=dumux/cmake.opts all
